/*
 * neo10SegDisplay library
 * 
 * Written by Ben Tenney
 * May 2, 2018
 * 
 * This is a library for creating neopixel based 10-segment displays
 * based off the FastLED neopixel library
 * 
 */



#ifndef NEO10SEGDISPLAY
  #define NEO10SEGDISPLAY
#include <FastLED.h>                                                    ///https://github.com/FastLED/FastLED.git
#include <string.h>
//put this into your code:
//#define DISPLAY_PIN 14



class neo10SegDisplay
{
  public:
    ///Constructors
    neo10SegDisplay(int numDigits=1, int LEDsPerSeg=1);
    ~neo10SegDisplay();
    ///Setup Functions
    template< uint8_t LED_PIN=14 >void begin() {FastLED.addLeds<WS2812, LED_PIN, RGB>(_LEDStrip, _numLEDs);};
    ///Display Functions
    //void display(String number);
    void display(const char* number);
  
  private:
    ///Variables
    CRGB* _LEDStrip;
    uint32_t _displayColor = CRGB::DarkBlue;
    int _numDigits;
    int _LEDPerSeg = 1;                                                 //to be used later
    int _numLEDs;
    const unsigned int _alphaNumCount = 22;
    const char* _alphaNumIndex = "0123456789ABCDEFbcd:. ";
    unsigned int _alphaNumMap[22] = {
      0B0000001111001010,    // 0
      0B0000000001001000,    // 1      77777
      0B0000001011010010,    // 2     8     6
      0B0000000011011010,    // 3     8  5  6
      0B0000000101011000,    // 4     8     6
      0B0000000110011010,    // 5      44444
      0B0000001110011010,    // 6     9     3
      0B0000000011001000,    // 7     9  2  3
      0B0000001111011010,    // 8     9     3
      0B0000000111011000,    // 9      11111  0
      0B0000001111011000,    // A
      0B0000001111011010,    // B
      0B0000001110000010,    // C
      0B0000001111001010,    // D
      0B0000001110010010,    // E
      0B0000001110010000,    // F
      0B0000001100011010,    // b
      0B0000001000010010,    // c
      0B0000001001011010,    // d
      0B0000000000100100,    // :
      0B0000000000000001,    // .
      0B0000000000000000     //
    };
};



#endif
