/*
 * Neopix10Seg library
 * 
 * Written by Ben Tenney
 * May 2, 2018
 * 
 * This is a library for creating neopixel based 10-segment displays
 * based off the FastLED neopixel library
 * 
 */



#include "neo10SegDisplay.h"
#include <FastLED.h>
#include <string.h>



////////////////////////////////////////////////////////////////////////
//Constructors
////////////////////////////////////////////////////////////////////////

neo10SegDisplay::neo10SegDisplay(int numDigits, int LEDsPerSeg)
{
  _numDigits=numDigits;
  //_LEDPerSeg=LEDsPerSeg;    //to be used later
  _numLEDs=_numDigits*_LEDPerSeg*10;
  //_displayColor = CRGB::DarkBlue;
  _LEDStrip = new CRGB[_numLEDs];
}

neo10SegDisplay::~neo10SegDisplay()
{
  delete _LEDStrip;
}



////////////////////////////////////////////////////////////////////////
//Setup Functions
////////////////////////////////////////////////////////////////////////

/*void neo10SegDisplay::begin()
{
  FastLED.addLeds<WS2812, DISPLAY_PIN, RGB>(_LEDStrip, _numLEDs);
}*/



////////////////////////////////////////////////////////////////////////
//Display Functions
////////////////////////////////////////////////////////////////////////

/*void neo10SegDisplay::display(String number)
{
  int pixCount = 0;
  char digit = ' ';
  for(int i=0; i<_numDigits; i++)
  {
    if(number.length()!=0)
    {
      digit = number[number.length()-1];
      number.remove(number.length()-1);
    }
    else
    {
      digit = ' ';
    }
    for(int index=0; index<_alphaNumCount; index++)
    {
      if(_alphaNumIndex[index] == digit)
      {
        for(int i=0; i<10; i++)
        {
          int isOn = 1 & (_alphaNumMap[index] >> i);
          _LEDStrip[pixCount+i] = _displayColor * isOn;
        }
        pixCount += 10;
      }
    }
  }
  FastLED.show();
}*/

void neo10SegDisplay::display(const char* number)
{
  int pixCount = 0;
  char digit = ' ';
  uint8_t length = 0;
  if(number != NULL)
    length = strlen(number);
  for(int i=0; i<_numDigits; i++)
  {
    if(length != 0)
    {
      digit = number[length - 1];
      length -= 1;
    }
    else
    {
      digit = ' ';
    }
    for(int index=0; index<_alphaNumCount; index++)
    {
      if(_alphaNumIndex[index] == digit)
      {
        for(int i=0; i<10; i++)
        {
          int isOn = 1 & (_alphaNumMap[index] >> i);
          _LEDStrip[pixCount+i] = _displayColor * isOn;
        }
        pixCount += 10;
      }
    }
  }
  FastLED.show();
}


